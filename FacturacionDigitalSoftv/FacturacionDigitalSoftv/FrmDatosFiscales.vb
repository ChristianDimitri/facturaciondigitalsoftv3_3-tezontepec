﻿Imports System.Data.SqlClient
Imports System.Text.RegularExpressions
Public Class FrmDatosFiscales

    'Variables bitacora
    Private Razon_social As String = Nothing
    Private RFC As String = Nothing
    Private CURP As String = Nothing
    Private Calle As String = Nothing
    Private Numero As String = Nothing
    Private entrecalles As String = Nothing
    Private Colonia As String = Nothing
    Private CodigoPostal As String = Nothing
    Private Estado As String = Nothing
    Private Ciudad As String = Nothing
    Private Telefono As String = Nothing
    Private Fax As String = Nothing
    Private Pais As String = Nothing
    'Fin Variables bitacora 

    Private Sub Llena_Compañias()
        Dim oIdAsociado As Long = 0
        Dim oIdCompania As Long = 0
        Try
            Class1.limpiaParametros()
            CmbCompania.DataSource = Class1.ConsultaDT("UspCompaniasMizar", ClassCFDI.MiConexion)
            CmbCompania.ValueMember = "id_compania"
            CmbCompania.DisplayMember = "razon_social"
            oIdAsociado = ClassCFDI.UspEsNuevoRel_Cliente_AsociadoCFD(ClassCFDI.Contrato, ClassCFDI.MiConexion)
            If oIdAsociado > 0 Then
                oIdCompania = ClassCFDI.Query_Asociado(" Select x2.id_compania from asociados x1 inner join companias x2 on x1.id_compania=x2.id_compania  Where id_asociado = " + CStr(oIdAsociado))
                CmbCompania.SelectedValue = oIdCompania
            End If
        Catch ex As Exception
            'MsgBox("El campo ID Grupo es Numerico", MsgBoxStyle.Exclamation, "Error")
            Llenalog(ex.Source.ToString & " " & ex.Message)

        End Try
    End Sub
    Private Sub FrmDatosFiscales_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        'Nos traemos los códigos postales
        Class1.limpiaParametros()
        cbCodigoPostal.DataSource = Class1.ConsultaDT("CodigosPostalesHL", ClassCFDI.MiConexion)

        Class1.limpiaParametros()
        cbUsoCFDI.DataSource = Class1.ConsultaDT("UsosCFDIHL", ClassCFDI.MiConexion)

        'Llena_Compañias()
        If ClassCFDI.OpcionCli = "C" Then
            Me.Panel1.Enabled = False
            Button1.Enabled = False
            Button2.Enabled = False
        ElseIf ClassCFDI.OpcionCli = "M" Then
            Me.Panel1.Enabled = True
            Button1.Enabled = True
            Button2.Enabled = True
        ElseIf ClassCFDI.OpcionCli = "N" Then
            Me.Panel1.Enabled = True
            Button1.Enabled = True
            Button2.Enabled = False
        End If
        If IsNumeric(ClassCFDI.GloContrato) = True Then
            Me.ContratoTextBox.Text = ClassCFDI.GloContrato
            Me.IVADESGLOSADOTextBox.Text = 1
        End If
        If ClassCFDI.OpcionCli = "M" Or ClassCFDI.OpcionCli = "C" Then
            Busca()
        End If
        
        damedatosbitacora()
    End Sub

    Private Sub Busca()

        Try
            Dim DT As New DataTable
            Class1.limpiaParametros()
            Class1.CreateMyParameter("@Contrato", SqlDbType.BigInt, ClassCFDI.GloContrato)
            DT = Class1.ConsultaDT("CONDatosFiscales", ClassCFDI.MiConexion)
            If DT.Rows.Count > 0 Then


                ContratoTextBox.Text = DT.Rows(0)(0).ToString
                IVADESGLOSADOTextBox.Text = DT.Rows(0)(1).ToString
                RAZON_SOCIALTextBox.Text = DT.Rows(0)(2).ToString
                RFCTextBox.Text = DT.Rows(0)(3).ToString
                CALLE_RSTextBox.Text = DT.Rows(0)(4).ToString
                NUMERO_RSTextBox.Text = DT.Rows(0)(5).ToString
                ENTRECALLESTextBox.Text = DT.Rows(0)(6).ToString
                COLONIA_RSTextBox.Text = DT.Rows(0)(7).ToString
                CIUDAD_RSTextBox.Text = DT.Rows(0)(8).ToString
                ESTADO_RSTextBox.Text = DT.Rows(0)(9).ToString
                CP_RSTextBox.Text = DT.Rows(0)(10).ToString
                TELEFONO_RSTextBox.Text = DT.Rows(0)(11).ToString
                FAX_RSTextBox.Text = DT.Rows(0)(12).ToString
                'CURPTextBox.Text = 'DT.Rows(0)(15).ToString
                NUMERO_intTextBox.Text = DT.Rows(0)(16).ToString

                cbUsoCFDI.SelectedValue = DT.Rows(0)(17).ToString

                cbCodigoPostal.SelectedValue = CP_RSTextBox.Text
                cbColonia.SelectedValue = COLONIA_RSTextBox.Text
            Else
                ContratoTextBox.Text = ClassCFDI.GloContrato
                IVADESGLOSADOTextBox.Text = "0"
                RAZON_SOCIALTextBox.Text = ""
                RFCTextBox.Text = ""
                CALLE_RSTextBox.Text = ""
                NUMERO_RSTextBox.Text = ""
                ENTRECALLESTextBox.Text = ""
                COLONIA_RSTextBox.Text = ""
                CIUDAD_RSTextBox.Text = ""
                ESTADO_RSTextBox.Text = ""
                CP_RSTextBox.Text = ""
                TELEFONO_RSTextBox.Text = ""
                FAX_RSTextBox.Text = ""
                'CURPTextBox.Text = ""
                NUMERO_intTextBox.Text = ""
            End If



            Consulta_Pais_Fiscal(ClassCFDI.GloContrato)
        Catch ex As System.Exception
            'System.Windows.Forms.MessageBox.Show(ex.Message)
            Llenalog(ex.Source.ToString & " " & ex.Message)
        End Try


    End Sub
    Private Sub Consulta_Pais_Fiscal(ByVal contrato As Long)
        Dim CON01 As New SqlConnection(ClassCFDI.MiConexion)
        Dim SQL As New SqlCommand()
        Try
            CON01.Open()
            SQL = New SqlCommand()
            With SQL
                .CommandText = "Consulta_Pais_Fiscal"
                .CommandTimeout = 0
                .Connection = CON01
                .CommandType = CommandType.StoredProcedure

                Dim PRM As New SqlParameter("@contrato", SqlDbType.BigInt)
                PRM.Direction = ParameterDirection.Input
                PRM.Value = contrato
                .Parameters.Add(PRM)

                Dim reader As SqlDataReader = .ExecuteReader()

                While (reader.Read)
                    TxtPais.Text = reader.GetValue(1)
                    EMail_TextBox.Text = reader.GetValue(2)
                End While
            End With
            CON01.Close()
        Catch ex As Exception
            'System.Windows.Forms.MessageBox.Show(ex.Message)
            Llenalog(ex.Source.ToString & " " & ex.Message)
        End Try
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        Dim CON As New SqlConnection(ClassCFDI.MiConexion)
        Dim oIdAsociado As Long = 0
        CON.Open()
        Try
            If Len(RAZON_SOCIALTextBox.Text) = 0 Then
                MsgBox("Favor de Agregar la Razón Social")

                Exit Sub
            ElseIf Len(RFCTextBox.Text) = 0 Then
                MsgBox("Favor de Agregar el RFC")
                Exit Sub
                'ElseIf Len(CURPTextBox.Text) = 0 Then
                '    'MsgBox("Favor de Agregar la CURP")
                '    Exit Sub
            ElseIf Len(CALLE_RSTextBox.Text) = 0 Then
                MsgBox("Favor de Agregar la Calle")
                Exit Sub
            ElseIf Len(NUMERO_RSTextBox.Text) = 0 Then
                MsgBox("Favor de Agregar el número ")
                Exit Sub
            ElseIf Len(cbColonia.Text) = 0 Then
                MsgBox("Favor de Agregar la Colonia ")
                Exit Sub
            ElseIf Len(cbCodigoPostal.Text) = 0 Then
                MsgBox("Favor de Agregar el Codigo Postal ")
                Exit Sub
            ElseIf Len(cbEstado.Text) = 0 Then
                MsgBox("Favor de Agregar el Estado")
                Exit Sub
            ElseIf Len(cbCiudad.Text) = 0 Then
                MsgBox("Favor de Agregar la Ciudad")
                Exit Sub
            ElseIf Len(cbPais.Text) = 0 Then
                MsgBox("Favor de Agregar el País")
                Exit Sub
            ElseIf Len(cbUsoCFDI.Text) = 0 Then
                MsgBox("Favor de Agregar el Uso CFDI")
                Exit Sub
            End If
            oIdAsociado = 0
            oIdAsociado = ClassCFDI.UspEsNuevoRel_Cliente_AsociadoCFD(RFCTextBox.Text, ClassCFDI.MiConexion)
            'If oIdAsociado = 0 Then
            '    oIdAsociado = ClassCFDI.Nuevo_Asociado("Select Max(id_asociado) from asociados ")
            'End If
            'If oIdAsociado > 0 Then
            'ClassCFDI.Alta_Datosfiscales_NEW(CmbCompania.SelectedValue, Me.RAZON_SOCIALTextBox.Text, Me.RFCTextBox.Text, CALLE_RSTextBox.Text, CStr(oIdAsociado), CP_RSTextBox.Text, COLONIA_RSTextBox.Text, EMail_TextBox.Text, ENTRECALLESTextBox.Text, ESTADO_RSTextBox.Text, CStr(oIdAsociado), CIUDAD_RSTextBox.Text, CIUDAD_RSTextBox.Text, NUMERO_RSTextBox.Text, TxtPais.Text, TELEFONO_RSTextBox.Text, FAX_RSTextBox.Text, CURPTextBox.Text, ClassCFDI.GloContrato, ClassCFDI.MiConexion)
            ClassCFDI.MZ_SPASOCIOADOSMIZAR(Me.RAZON_SOCIALTextBox.Text, Me.RFCTextBox.Text, CALLE_RSTextBox.Text, CStr(oIdAsociado), cbCodigoPostal.Text, cbColonia.Text, EMail_TextBox.Text, ENTRECALLESTextBox.Text, cbEstado.Text, CStr(oIdAsociado), cbCiudad.Text, cbCiudad.Text, NUMERO_RSTextBox.Text, NUMERO_intTextBox.Text, cbPais.Text, TELEFONO_RSTextBox.Text, FAX_RSTextBox.Text, "", ClassCFDI.GloContrato, "No Especificado", ClassCFDI.MiConexion)
            'End If


            If ClassCFDI.oAlta_Datosfiscales = True Then
                Me.Validate()
                Class1.limpiaParametros()
                Class1.CreateMyParameter("@Contrato", SqlDbType.BigInt, ClassCFDI.GloContrato)
                Class1.CreateMyParameter("@IVADESGLOSADO", SqlDbType.TinyInt, IVADESGLOSADOTextBox.Text)
                Class1.CreateMyParameter("@RAZON_SOCIAL", SqlDbType.VarChar, RAZON_SOCIALTextBox.Text, 150)
                Class1.CreateMyParameter("@RFC", SqlDbType.VarChar, RFCTextBox.Text, 50)
                Class1.CreateMyParameter("@CALLE_RS", SqlDbType.VarChar, CALLE_RSTextBox.Text, 150)
                Class1.CreateMyParameter("@NUMERO_RS", SqlDbType.VarChar, NUMERO_RSTextBox.Text, 50)
                Class1.CreateMyParameter("@ENTRECALLES", SqlDbType.VarChar, ENTRECALLESTextBox.Text, 150)
                Class1.CreateMyParameter("@COLONIA_RS", SqlDbType.VarChar, cbColonia.Text, 150)
                Class1.CreateMyParameter("@CIUDAD_RS", SqlDbType.VarChar, cbCiudad.Text, 150)
                Class1.CreateMyParameter("@ESTADO_RS", SqlDbType.VarChar, cbEstado.Text, 150)
                Class1.CreateMyParameter("@CP_RS", SqlDbType.VarChar, cbCodigoPostal.Text, 20)
                Class1.CreateMyParameter("@TELEFONO_RS", SqlDbType.VarChar, TELEFONO_RSTextBox.Text, 50)
                Class1.CreateMyParameter("@FAX_RS", SqlDbType.VarChar, FAX_RSTextBox.Text, 50)
                Class1.CreateMyParameter("@TIPO", SqlDbType.VarChar, "", 1)
                Class1.CreateMyParameter("@CURP", SqlDbType.VarChar, "", 50)
                Class1.CreateMyParameter("@NUMEROINT_RS", SqlDbType.VarChar, NUMERO_intTextBox.Text, 50)
                Class1.CreateMyParameter("@id_UsoCFDI", SqlDbType.VarChar, cbUsoCFDI.SelectedValue, 50)
                Class1.Inserta("NUEDatosFiscales", ClassCFDI.MiConexion)

                Guarda_Rel_DatosFiscales_Pais(ClassCFDI.GloContrato, Me.cbPais.Text, EMail_TextBox.Text)

                'MsgBox(ClassCFDI.mensaje5)
                guardabitacora(ClassCFDI.GloContrato)
                Me.Close()
            End If
        Catch ex As System.Exception
            'System.Windows.Forms.MessageBox.Show(ex.Message)
            Llenalog(ex.Source.ToString & " " & ex.Message)
        End Try
        CON.Close()
    End Sub
    Private Sub Guarda_Rel_DatosFiscales_Pais(ByVal contrato As Long, ByVal Pais As String, ByVal oEmail As String)
        Dim CON01 As New SqlConnection(ClassCFDI.MiConexion)
        Dim SQL As New SqlCommand()
        Try
            CON01.Open()
            SQL = New SqlCommand()
            With SQL
                .CommandText = "Guarda_Rel_DatosFiscales_Pais"
                .Connection = CON01
                .CommandTimeout = 0
                .CommandType = CommandType.StoredProcedure

                Dim prm As New SqlParameter("@contrato", SqlDbType.BigInt)
                prm.Value = contrato
                prm.Direction = ParameterDirection.Input
                .Parameters.Add(prm)

                prm = New SqlParameter("@Pais", SqlDbType.VarChar, 150)
                prm.Value = Pais
                prm.Direction = ParameterDirection.Input
                .Parameters.Add(prm)

                prm = New SqlParameter("@Email", SqlDbType.VarChar, 100)
                prm.Value = oEmail
                prm.Direction = ParameterDirection.Input
                .Parameters.Add(prm)

                Dim i As Integer = .ExecuteNonQuery()
            End With
            CON01.Close()
        Catch ex As Exception
            'System.Windows.Forms.MessageBox.Show(ex.Message)
            ' Llenalog(ex.Source.ToString & " " & ex.Message)
            Llenalog(ex.Source.ToString & " " & ex.Message)
        End Try
    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        Try

        
        Class1.limpiaParametros()
        Class1.CreateMyParameter("@contrato", SqlDbType.BigInt, ClassCFDI.GloContrato)
        Class1.Inserta("BORDatosFiscales", ClassCFDI.MiConexion)
        'MsgBox(ClassCFDI.mensaje6)
        UspBorrarDatosFiscalesPais()
        Me.TxtPais.Text = ""
            ClassCFDI.Borrar_Datosfiscales(RFCTextBox.Text, ClassCFDI.MiConexion)
        ClassCFDI.Usp_ED_BorraRel_Cliente_AsociadoCFD(ClassCFDI.GloContrato, ClassCFDI.MiConexion)
            Me.Close()
        Catch ex As Exception
            'ex.Source.ToString & " " & ex.Message
            Llenalog(ex.Source.ToString & " " & ex.Message)
        End Try
    End Sub
    Private Sub damedatosbitacora()
        Try
            If ClassCFDI.OpcionCli = "M" Then
                Razon_social = Me.RAZON_SOCIALTextBox.Text
                RFC = Me.RFCTextBox.Text
                CURP = "" 'Me.CURPTextBox.Text
                Calle = Me.CALLE_RSTextBox.Text
                Numero = Me.NUMERO_RSTextBox.Text
                entrecalles = Me.ENTRECALLESTextBox.Text
                Colonia = Me.COLONIA_RSTextBox.Text
                CodigoPostal = Me.CP_RSTextBox.Text
                Estado = Me.ESTADO_RSTextBox.Text
                Ciudad = Me.CIUDAD_RSTextBox.Text
                Telefono = Me.TELEFONO_RSTextBox.Text
                Fax = Me.FAX_RSTextBox.Text
                Pais = Me.TxtPais.Text
            End If
        Catch ex As Exception
            'System.Windows.Forms.MessageBox.Show(ex.Message)
            Llenalog(ex.Source.ToString & " " & ex.Message)
        End Try
    End Sub
    Private Sub UspBorrarDatosFiscalesPais()
        Try
            Class1.limpiaParametros()
            Class1.CreateMyParameter("@contrato", SqlDbType.BigInt, ClassCFDI.GloContrato)
            Class1.Inserta("UspBorrarDatosFiscalesPais", ClassCFDI.MiConexion)
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    Private Sub guardabitacora(ByVal contrato As Integer)
        Try
            If ClassCFDI.gloOpcionCli = "M" Then
                'Razon_social = Me.RAZON_SOCIALTextBox.Text
                bitsist(ClassCFDI.GloUsuario, contrato, ClassCFDI.LocGloSistema, Me.Name, Me.RAZON_SOCIALTextBox.Name, Razon_social, Me.RAZON_SOCIALTextBox.Text, ClassCFDI.LocClv_Ciudad)
                'RFC = Me.RFCTextBox.Text
                bitsist(ClassCFDI.GloUsuario, contrato, ClassCFDI.LocGloSistema, Me.Name, Me.RFCTextBox.Name, RFC, Me.RFCTextBox.Text, ClassCFDI.LocClv_Ciudad)
                'CURP = Me.CURPTextBox.Text
                'bitsist(ClassCFDI.GloUsuario, contrato, ClassCFDI.LocGloSistema, Me.Name, Me.CURPTextBox.Name, CURP, Me.CURPTextBox.Text, ClassCFDI.LocClv_Ciudad)
                'Calle = Me.CALLE_RSTextBox.Text
                bitsist(ClassCFDI.GloUsuario, contrato, ClassCFDI.LocGloSistema, Me.Name, Me.CALLE_RSTextBox.Name, Calle, Me.CALLE_RSTextBox.Text, ClassCFDI.LocClv_Ciudad)
                'Numero = Me.NUMERO_RSTextBox.Text
                bitsist(ClassCFDI.GloUsuario, contrato, ClassCFDI.LocGloSistema, Me.Name, Me.NUMERO_RSTextBox.Name, Numero, Me.NUMERO_RSTextBox.Text, ClassCFDI.LocClv_Ciudad)
                'entrecalles = Me.ENTRECALLESTextBox.Text
                bitsist(ClassCFDI.GloUsuario, contrato, ClassCFDI.LocGloSistema, Me.Name, Me.ENTRECALLESTextBox.Name, entrecalles, Me.ENTRECALLESTextBox.Text, ClassCFDI.LocClv_Ciudad)
                'Colonia = Me.COLONIA_RSTextBox.Text
                bitsist(ClassCFDI.GloUsuario, contrato, ClassCFDI.LocGloSistema, Me.Name, Me.COLONIA_RSTextBox.Name, Colonia, Me.COLONIA_RSTextBox.Text, ClassCFDI.LocClv_Ciudad)
                'CodigoPostal = Me.CP_RSTextBox.Text
                bitsist(ClassCFDI.GloUsuario, contrato, ClassCFDI.LocGloSistema, Me.Name, Me.CP_RSTextBox.Name, CodigoPostal, Me.CP_RSTextBox.Text, ClassCFDI.LocClv_Ciudad)
                'Estado = Me.ESTADO_RSTextBox.Text
                bitsist(ClassCFDI.GloUsuario, contrato, ClassCFDI.LocGloSistema, Me.Name, Me.ESTADO_RSTextBox.Name, Estado, Me.ESTADO_RSTextBox.Text, ClassCFDI.LocClv_Ciudad)
                'Ciudad = Me.CIUDAD_RSTextBox.Text
                bitsist(ClassCFDI.GloUsuario, contrato, ClassCFDI.LocGloSistema, Me.Name, Me.CIUDAD_RSTextBox.Name, Ciudad, Me.CIUDAD_RSTextBox.Text, ClassCFDI.LocClv_Ciudad)
                'Pais = Me.txtPais
                bitsist(ClassCFDI.GloUsuario, contrato, ClassCFDI.LocGloSistema, Me.Name, "Pais Fiscal", Pais, Me.TxtPais.Text, ClassCFDI.LocClv_Ciudad)
                'Telefono = Me.TELEFONO_RSTextBox.Text
                bitsist(ClassCFDI.GloUsuario, contrato, ClassCFDI.LocGloSistema, Me.Name, Me.TELEFONO_RSTextBox.Name, Telefono, Me.TELEFONO_RSTextBox.Text, ClassCFDI.LocClv_Ciudad)
                'Fax = Me.FAX_RSTextBox.Text
                bitsist(ClassCFDI.GloUsuario, contrato, ClassCFDI.LocGloSistema, Me.Name, Me.FAX_RSTextBox.Name, Fax, Me.FAX_RSTextBox.Text, ClassCFDI.LocClv_Ciudad)
            ElseIf ClassCFDI.OpcionCli = "N" Then
                bitsist(ClassCFDI.GloUsuario, contrato, ClassCFDI.LocGloSistema, Me.Name, "Se Capturaron Los Datos Fiscales Del Cliente", "", "Se Capturaron Los Datos Fiscales Del Cliente", ClassCFDI.LocClv_Ciudad)
            End If
        Catch ex As Exception
            'System.Windows.Forms.MessageBox.Show(ex.Message)
            Llenalog(ex.Source.ToString & " " & ex.Message)
        End Try
    End Sub

    Public Shared Sub bitsist(ByVal usuario As String, ByVal contrato As Long, ByVal sistema As String, ByVal pantalla As String, ByVal control As String, ByVal valorant As String, ByVal valornuevo As String, ByVal clv_ciudad As String)
        Try
            Class1.limpiaParametros()
            Class1.CreateMyParameter("@usuario", SqlDbType.VarChar, usuario)
            Class1.CreateMyParameter("@contrato", SqlDbType.VarChar, contrato)
            Class1.CreateMyParameter("@sistema", SqlDbType.VarChar, sistema)
            Class1.CreateMyParameter("@pantalla", SqlDbType.VarChar, pantalla)
            Class1.CreateMyParameter("@control", SqlDbType.VarChar, control)
            Class1.CreateMyParameter("@valorant", SqlDbType.VarChar, valorant)
            Class1.CreateMyParameter("@valornuevo", SqlDbType.VarChar, valornuevo)
            Class1.CreateMyParameter("@clv_ciudad", SqlDbType.VarChar, clv_ciudad)
            Class1.Inserta("Inserta_MovSist", ClassCFDI.MiConexion)
        Catch ex As Exception
            'System.Windows.Forms.MessageBox.Show(ex.Message)
            Llenalog(ex.Source.ToString & " " & ex.Message)
        End Try
    End Sub

    Private Sub Button5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button5.Click
        Me.Close()
    End Sub

    Private Sub RFCTextBox_TextChanged(sender As Object, e As EventArgs) Handles RFCTextBox.TextChanged

    End Sub

    Private Sub RFCTextBox_Validating(sender As Object, e As ComponentModel.CancelEventArgs) Handles RFCTextBox.Validating
        If RFCTextBox.Text <> String.Empty Then
            If Regex.IsMatch(RFCTextBox.Text.Trim, "^([A-Z\s]{4})\d{6}([A-Z\w]{3})$") = False Then
                MsgBox("El Registro no es válido. El formato correcto es: LLLL######LL ó LLLL######LLL. L=Letra, #=Número.")
            End If
        End If

    End Sub

    Private Sub cbCodigoPostal_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cbCodigoPostal.SelectedIndexChanged
        Try
            Class1.limpiaParametros()
            Class1.CreateMyParameter("@id_CodigoPostal", SqlDbType.VarChar, cbCodigoPostal.SelectedValue)
            cbColonia.DataSource = Class1.ConsultaDT("ObtieneColoniasHL", ClassCFDI.MiConexion)

            Class1.limpiaParametros()
            Class1.CreateMyParameter("@id_CodigoPostal", SqlDbType.VarChar, cbCodigoPostal.SelectedValue)
            cbEstado.DataSource = Class1.ConsultaDT("ObtieneEstadosHL", ClassCFDI.MiConexion)

            Class1.limpiaParametros()
            Class1.CreateMyParameter("@id_CodigoPostal", SqlDbType.VarChar, cbCodigoPostal.SelectedValue)
            Class1.CreateMyParameter("@id_Estado", SqlDbType.VarChar, cbEstado.SelectedValue)
            cbCiudad.DataSource = Class1.ConsultaDT("ObtieneMunicipiosHL", ClassCFDI.MiConexion)

            Class1.limpiaParametros()
            Class1.CreateMyParameter("@id_CodigoPostal", SqlDbType.VarChar, cbCodigoPostal.SelectedValue)
            cbPais.DataSource = Class1.ConsultaDT("ObtienePaisesHL", ClassCFDI.MiConexion)
        Catch ex As Exception

        End Try
    End Sub

    Private Sub cbColonia_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cbColonia.SelectedIndexChanged

    End Sub
End Class